# Member-Newsletter (Newsletter-Registrierung nach Member-Gruppen-Einstellung)

Diese Erweiterung ermöglicht es, in den Member-Gruppen-Einstellungen Newsletter-Channel zuzuordnen. Wenn der Frontend-User sich registriert hat er die Auswahl sich zu verschiedenen Gruppen anzumelden. Bei der Registrierung wird dann je nach Gruppe die jeweilig zugeordneten Newslettern zum Member eingetragen.



